---
title: "Case Study I"
output: html_notebook
---

This is an [R Markdown](http://rmarkdown.rstudio.com) Notebook. When you execute code within the notebook, the results appear beneath the code. 

Try executing this chunk by clicking the *Run* button within the chunk or by placing your cursor inside it and pressing *Ctrl+Shift+Enter*. 


Here we include the ViSCAR code that implements each step of the case study I, presented in the main text


1.  Dataset general inspection. ViscaR code that loads the SuperSegger dataset and creates and plots its FLT and FDT representation. Then it computes the number of cell instants of the movie, average cell instant size and width, the number of cells and the number of generations. Finally, it plots the cell instants distribution (across frames) and the cells distribution (across generations) of the cell movie.
2.  Subpopulation selection. ViscaR code that selects and visualize on the FLT  a) the lineage tree of a progenitor cell (cell 6) and b) cell instants which are in contact with other cells and are outliers in terms of cell area.
3.  Analytics.


#### Importing data
Use the import_ss function to demonstrate that ViSCAR can read output of other state-of-the-art software rather than BaSCA, e.g. SuperSegger here. 
```{r, echo = T, results = 'hide'}
ssPath <- "../../Datasets/cell"
#import SuperSegger output
myLists <- import_ss(path = ssPath, pixelR = 0.0594)
```
<br/>

#### Create FLT from the imported data
```{r, echo = T, results = 'hide'}
myFLT <- createFLT(cell_list = myLists$cell_list, Ncols = myLists$Ncols)
```
<br/>

#### Create FDT from FLT
```{r, echo = T, results = 'hide'}
myFDT <- createFDT(LTmain = myFLT$LTmain, minLife = 5, frameR = 1)
FLT <- myFDT$LTmain
FDT <- myFDT$DTmain
```
<br/>

#### Reproduce subfigures of Fig 7 of the main text. 
```{r, tidy=TRUE, tidy.opts=list(blank=TRUE, width.cutoff=60)}

#plot the FLT of the input data and export it into Fig 7a file
plot_tree(tree = FLT, treeT = "LT", attrC = "generation", NC = 3, attrS = "", cellsC = "", colorCol = FALSE, showLabels = FALSE, circular = FALSE, showLegends = TRUE, sizeV = 2, sizeE = 1, save = TRUE, savePars = list(w = 6000, h = 6000, res = 300, path = getwd(), name = "Fig 7a"))

#plot the FDT of the input data and exports it into Fig 7b file
plot_tree(tree = FDT, treeT = "DT", attrC = "", attrS = "isConsidered", cellsC = get_cells(tree = FDT, treeT = "DT", type = "nr"), colorCol = FALSE, showLabels = TRUE, sizeLabel = 1, circular = FALSE, showLegends = TRUE, sizeV = 4, sizeE = 3, save = TRUE, savePars = list(w = 6000, h = 6000, res = 300, path = getwd(), name = "Fig 7b"))

#compute and print the number of the movie's cell instances
length(get_cells(tree = FLT, treeT = "LT", type = "nr"))

#compute and print the average length of the movie's cell instances
get_attr_stats(tree = FLT, treeT = "LT", attr = "length", stat = "mean")

#compute and print the average width of the movie's cell instances
get_attr_stats(tree = FLT, treeT = "LT", attr = "width", stat = "mean")

#compute and print the number of movie's cells
length(get_cells(tree = FDT, treeT = "DT", type = "nr"))
myFDT$Ngens

#plot the distribution of cell instances and export the corresponding barchart into Fig 7c
plot_Ncells(tree = FLT, treeT = "LT", grouped = "frame", groups = -1, Ngroups = 60, sizeT = 18, save = TRUE, savePars = list(w = 4000, h = 2000, res = 200, path = getwd(), name = "Fig 7c"))

#plots the distribution of cells and export the corresponding barchart into Fig 7d
plot_Ncells(tree = FDT, treeT = "DT", grouped = "gen", groups = -1, Ngroups = 3, save = TRUE, sizeT=7, savePars = list(w = 1000, h = 1000, res = 200, path = getwd(), name = "Fig 7d"))

```
<br/>

#### Reproduce subfigures of Fig 8 of the main text. 
```{r, tidy=TRUE, tidy.opts=list(blank=TRUE, width.cutoff=60)}

#extract the subtree of cell 6
subLTa <- extract_branch(tree = FLT, cell = get_LT_cell(DT = FDT, cell = "6", instant = 1))$branch

#plot the FLT coloring only the nodes corresponding to the subtree of cell 6 and export it into Fig 8a file
plot_tree(tree = FLT, treeT = "LT", attrC = "", attrS = "", cellsC = get_cells(tree = subLTa, treeT = "LT", type = "all"), colorCol = FALSE, showLabels = FALSE, circular = FALSE, showLegends = FALSE, sizeV = 2, sizeE = 1, save = TRUE, savePars = list(w = 6000, h = 6000, res = 300, path = getwd(), name = "Fig 8a"))
```

```{r, eval=FALSE}
#create criteria so as to select specific cell instances on the FLT
trees <- list()
criteria <- list()
#criterion 1->contactHist attribute must be true
criteria[[1]] <- list(attr = "contactHist", val = TRUE, op = "==")

#criterion 2->area must be less than mean(area)-sd(area)
criteria[[2]] <- list(attr = "area", val = get_attr_stats(tree = FLT, treeT = "LT", attr = "area", stat = "mean")$value - get_attr_stats(tree = FLT, treeT = "LT", attr = "area", stat = "sd")$value, op = "<")

#get the subtree that fulfills the criteria 1 and 2
trees[[1]] <- select_subtree(tree = FLT, criteria = criteria)

#redefine criterion 2 -> area must be greater than mean(area)+sd(area)
criteria[[2]] <- list(attr = "area", val = get_attr_stats(tree = FLT, treeT = "LT", attr = "area", stat = "mean")$value + get_attr_stats(tree = FLT, treeT = "LT", attr = "area", stat = "sd")$value, op = ">")

#get the subtree that fulfills the criteria 1 and 2
trees[[2]] <- select_subtree(tree = FLT, criteria = criteria)

#merge the subtrees of cell instance fullfilled the criteria above 
subLTb <- unite_trees(trees = trees)
```

```{r, echo=FALSE}
#create criteria so as to select specific cell instances on the FLT
trees <- list()
criteria <- list()
#criterion 1->contactHist attribute must be true
criteria[[1]] <- list(attr = "contactHist", val = TRUE, op = "==")

#criterion 2->area must be less than mean(area)-sd(area)
criteria[[2]] <- list(attr = "area", val = get_attr_stats(tree = FLT, treeT = "LT", attr = "area", stat = "mean")$value - get_attr_stats(tree = FLT, treeT = "LT", attr = "area", stat = "sd")$value, op = "<")

#get the subtree that fulfills the criteria 1 and 2
trees[[1]] <- select_subtree(tree = FLT, criteria = criteria)

#redefine criterion 2 -> area must be greater than mean(area)+sd(area)
criteria[[2]] <- list(attr = "area", val = get_attr_stats(tree = FLT, treeT = "LT", attr = "area", stat = "mean")$value + get_attr_stats(tree = FLT, treeT = "LT", attr = "area", stat = "sd")$value, op = ">")

#get the subtree that fulfills the criteria 1 and 2
trees[[2]] <- select_subtree(tree = FLT, criteria = criteria)

#merge the subtrees of cell instance fullfilled the criteria above 
subLTb <- unite_trees(trees = trees)
```

```{r, tidy=TRUE, tidy.opts=list(blank=TRUE, width.cutoff=60)}
#plot the FLT coloring only the nodes corresponding to the subLTb and export it into Fig 8b file
plot_tree(tree = FLT, treeT = "LT", attrC = "", attrS = "", cellsC = get_cells(tree = subLTb, treeT = "LT", type = "all"), colorCol = FALSE, showLabels = FALSE, circular = FALSE, showLegends = FALSE, sizeV = 2, sizeE = 1,save = TRUE, savePars = list(w = 6000, h = 6000, res = 300, path = getwd(), name = "Fig 8b"))

```
<br/>

#### Reproduce subfigures of Fig S1 of additional file 1.
```{r, tidy=TRUE, tidy.opts=list(blank=TRUE, width.cutoff=60)}

#plot 2D scatter plot of cell instance length vs area, contactHist defines the mark type and export it into Fig 9a file
plot_dot_attr2(tree = FLT, treeT = "LT", attr1 = "length", unit1 = "μm", attr2 = "area", unit2 = "μm,2", attrC = "fluorescenceInt1.mean", unitC = "AU", attrS = "contactHist", save = TRUE, savePars = list(w = 7000, h = 5000, res = 300, path = getwd(), name = "Fig S1a"))

#plot violin plot of cell birth length and export it into Fig 9b file
plot_viobox_attr(tree = FDT, treeT = "DT", attr = "length_birth", unit = "μm", grouped = "pop", type = "vio", xRange = c(0, 5), save = TRUE, savePars = list(w = 2500, h = 2000, res = 200, path = getwd(), name = "Fig S1b"))

#plot violin plot of cell division length and export it into Fig 9c file
plot_viobox_attr(tree = FDT, treeT = "DT", attr = "length_division", unit = "μm,2", grouped = "pop", type = "vio", xRange = c(0, 3), save = TRUE, savePars = list(w = 2500, h = 2000, res = 200, path = getwd(), name = "Fig S1c"))
```
<br/>

#### Add attribute fiting data in the FDT
Compute and store the elongation rate (by fitting an exponential model) to the FDT
```{r, echo = T, results = 'hide', tidy=TRUE, tidy.opts=list(blank=TRUE, width.cutoff=60)}
result <- add_attr_growth_fit_pars(LT = FLT, DT = FDT, attr = "length", model = "exp", frameR = 1)
FDT <- result$DT
cells_failed <- result$cells #a few cells may fail to fit the exponential modelw 
```
<br/>

#### Reproduce subfigures of Fig 9 of the main text.
```{r, tidy=TRUE, tidy.opts=list(blank=TRUE, width.cutoff=60)}
#plot length trajectories of all the cells and export it to Fig 10a file.
plot_growth_attr(DT = FDT, LT = FLT, attr = "length", unit = "μm", cells = "all", Nframes = 60, save = TRUE, savePars = list(w = 2500, h = 2000, res = 200, path = getwd(), name = "Fig 9a"))

#plot length trajectories of the cells failed previously to fit the exponential model and export it to Fig 10b file.
plot_growth_attr(DT = FDT, LT = FLT, attr = "length", unit = "μm", cells = cells_failed, Nframes = 60, save = TRUE, savePars = list(w = 2500, h = 2000, res = 200, path = getwd(), name = "Fig 9b"))

#plot exponential fit trajectories of the cells fitted successfully the exponential model and average exponential fit trajectory of the cells fitted successfully the exponential model and export them to Fig 10c_d file.
plot_growth_attr_fit(DT = FDT, LT = FLT, attr = "length", unit = "μm", model = "exp", grouped = "pop", attrC = "length_expRMSE", dur = 0.8, sizeL = 0.7, save = TRUE, savePars = list(w = 2500, h = 2000, res = 200, path = getwd(), name = "Fig 9c_d"))

```
<br/>

#### Reproduce subfigures of Fig S2 and Fig S3 of additional file 1.
```{r, tidy=TRUE, tidy.opts=list(blank=TRUE, width.cutoff=60)}
criteria <- list()
criteria[[1]] <- list(attr = "length_k", val = get_attr_stats(tree = FDT, treeT = "DT", attr = "length_k", stat = "mean")$value - 2*get_attr_stats(tree = FDT, treeT = "DT", attr = "length_k", stat = "sd")$value, op = "<")
subDT <- select_subtree(tree = FDT, criteria = criteria)

#plot the histogram of cell elongation rate and export it to Fig S2b
plot_hist_attr(tree = FDT, treeT = "DT", attr = "length_k", unit = "μm/h", grouped = "pop", Nbins = 10, save = TRUE, savePars = list(w = 2500, h = 2000, res = 200, path = getwd(), name = "Fig S2a"))

plot_growth_attr(DT = FDT, LT = FLT, attr = "length", unit = "μm", cells = get_cells(tree = subDT, treeT = "DT", type = "all"), Nframes = 60, save = TRUE, savePars = list(w = 2500, h = 2000, res = 200, path = getwd(), name = "Fig S2b"))

criteria <- list()
criteria[[1]] <- list(attr = "length_expRMSE", val = 0.04, op = ">")
subDT <- select_subtree(tree = FDT, criteria = criteria)

#plot the histogram of rmse of the exponential fit for each cell and export it to Fig S2c
plot_hist_attr(tree = FDT, treeT = "DT", attr = "length_expRMSE", unit = "", grouped = "pop", Nbins = 10, save = TRUE, savePars = list(w = 2500, h = 2000, res = 200, path = getwd(), name = "Fig S2c"))

#plot length trajectories of the cells that were previously selected and export it to Fig S2d file.
plot_growth_attr(DT = FDT, LT = FLT, attr = "length", unit = "μm", cells = get_cells(tree = subDT, treeT = "DT", type = "all"), Nframes = 60, save = TRUE, savePars = list(w = 2500, h = 2000, res = 200, path = getwd(), name = "Fig S2d"))

#plot length trajectories of each cell that was previously selected and export it to different files, subfigures of Fig S3 in S1 Text.
plot_growth_attr_cell(DT = FDT, LT = FLT, attr = "length", unit = "μm", cells = get_cells(tree = subDT, treeT = "DT", type = "all"), model = "exp", frameR = 1, save = TRUE, savePars = list(w = 2500, h = 1500, res = 200, path = getwd(), name = "Fig S3"))
```
<br/>

#### Reproduce subfigures of Fig S4 of additional file 1.
```{r, tidy=TRUE, tidy.opts=list(blank=TRUE, width.cutoff=60)}

#compute best fit pdf of elongation rate data and plot the fitted pdf. Estimated parameters will be printed below
plot_pdf_attr(tree = FDT, treeT = "LT", attr = "length_k", unit = "μm/h", grouped = "pop", model = "auto")

#compute normal distribution fit of elongation rate data and plot the fitted pdf and export it to Fig 12a subfigure
plot_pdf_attr(tree = FDT, treeT = "LT", attr = "length_k", unit = "μm/h", grouped = "pop", model = "norm", yRange = c(0,2.2), save = TRUE, savePars = list(w = 2500, h = 2000, res = 200, path = getwd(), name = "Fig S4a"))

#compute gamma distribution fit of elongation rate data and plot the fitted pdf and export it to Fig 12b subfigure
plot_pdf_attr(tree = FDT, treeT = "LT", attr = "length_k", unit = "μm/h", grouped = "pop", model = "gamma", yRange = c(0,2.2), save = TRUE, savePars = list(w = 2500, h = 2000, res = 200, path = getwd(), name = "Fig S4b"))

#compute log-normal distribution fit of elongation rate data and plot the fitted pdf and export it to Fig 12c subfigure
plot_pdf_attr(tree = FDT, treeT = "LT", attr = "length_k", unit = "μm/h", grouped = "pop", model = "lnorm", yRange = c(0,2.2), save = TRUE, savePars = list(w = 2500, h = 2000, res = 200, path = getwd(), name = "Fig S4c"))

```

When you save the notebook, an HTML file containing the code and output will be saved alongside it (click the *Preview* button or press *Ctrl+Shift+K* to preview the HTML file).

The preview shows you a rendered HTML copy of the contents of the editor. Consequently, unlike *Knit*, *Preview* does not run any R code chunks. Instead, the output of the chunk when it was last run in the editor is displayed.
