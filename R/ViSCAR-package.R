#' Overview of the ViSCAR package
#'
#' ViSCAR is an R package for the statistical analysis and visualization of single-cell data derived from 
#' the image analysis of time-lapse bacterial cell-movies. The package enables users to explore the 
#' spatiotemporal trends of single-cell attributes, discover possible epigenetic effects across 
#' generations, and even identify and correct segmentation and tracking errors.
#'
#' ViSCAR was initially created to extend the BaSCA image analysis pipeline's capabilities by adding
#' single-cell analytics, advanced visualization, and error correction capabilities. However, most of
#' these capabilities are general and independent of the image analysis pipeline used to generate the 
#' dataset. Therefore, ViSCAR can be used for data derived using other image analysis software, such 
#' as \emph{SuperSegger} and \emph{Oufti}, or even synthetic bacterial cell-movies developed with tools such as the 
#' \emph{CellModeler} for in-silico simulation
#'
#' @section Data import:
#' ViSCAR supports various input file formats for the analyzed cell-movies.
#' \cr\cr
#' Use \code{\link{import_basca}}, \code{\link{import_oufti}} or \code{\link{import_ss}}
#' to import data exported by the named software.
#' These functions automatically convert the input file(s) into a cell list (and colony list),
#' containing all the cell instants (and colony instants) of the movie, respectively.
#' \cr\cr
#' Such structures can also be directly imported from custom-made \code{.json} files,
#' provided that they have the appropriate format.
#' See \code{\link{import_json}} for details.
#'
#' @section FLT/FDT representation of cell-movies:
#' Once the movie's cell list is loaded, 
#' use \code{\link{createFLT}} to transform it into a Forest of Lineage Trees (FLT) data structure. 
#' A lineage tree (LT) node represents a cell instant (i.e., cell at a specific frame/instant of its lifespan). 
#' A continuous segment (sequence) of LT nodes between two succes- sive cell divisions represents the lifespan of a cell.
#' \cr\cr
#' Suppose one reduces LT cell segments down to a single node. 
#' In that case, she obtains the Forest of Division Trees (FDT) data structure, 
#' capturing only each cell’s division event and summarizing its lifespan. 
#' A division tree (DT) node represents a cell (i.e., cell at its full lifespan). 
#' See \code{\link{createFDT}} for details.
#' \cr\cr
#' These tree data structures are objects of class \code{"igraph"} and are the core data structures of the package.
#'
#' @section Attributes:
#' Single-cell attributes are divided into two broad categories:
#' \itemize{
#' \item cell instant attributes, that may change value at each time point.
#' These attributes are extracted by the software, loaded into the cell list and
#' finally stored as node attributes in the FLT by \code{\link{createFLT}}.
#' \cr\cr
#' Some other values as also stored as node attributes in the FLT when
#' \code{\link{add_attr_roc}} or \code{\link{createFDT}} are called.
#' See the documentation of each function for more details.
#' \item cell life attributes characterize a cell’s whole lifespan.
#' These attributes are estimated by \code{\link{createFDT}} and are stored as node attributes in the FDT.
#' \cr\cr
#' Some other values as also stored as node attributes in the FDT
#' when \code{\link{add_attr_growth_fit_pars}} is called.
#' See the documentation of the function for more details.
#' }
#'
#' @section Analytics:
#' ViSCAR allows users to perform statistical analysis of single-cell attributes at multiple levels of organization
#' (whole community, sub-population, colonies, generations, subtrees of individual colonies, etc.).
#' Analytics capabilities are categorized into:
#' \itemize{
#' \item statistics (\code{\link{get_attr_stats}}, \code{\link{plot_hist_attr}}, \code{\link{plot_pdf_attr}},
#' \code{\link{plot_viobox_attr}}, \code{\link{plot_time_attr}})
#' \item scatterplots for correlating attributes (\code{\link{plot_dot_attr2}}, \code{\link{plot_dot_attr3}},
#' \code{\link{plot_dot_time_attr}}, \code{\link{plot_dot_attr2_gen2}}, \code{\link{plot_dot_attr_fam}})
#' \item estimation of growth curves (\code{\link{plot_baranyi}}, \code{\link{add_attr_growth_fit_pars}},
#' \code{\link{plot_growth_attr_fit}}, \code{\link{plot_growth_attr}}, \code{\link{plot_growth_attr_cell}})
#' }
#'
#' @section Visualization:
#' ViSCAR provides two different ways for visualization:
#' \enumerate{
#' \item \code{\link{plot_tree}} for visualizing a lineage or generation tree
#' \item \code{\link{create_movie}} for animating the segmented cells by creating videos
#' }
#' Color can be used to map a cell instant/life attribute and
#' capture its variability across cells, colonies, frames or generations.
#' \cr\cr
#' The user can also monitor how the life of a selected cell evolves in the movie using \code{\link{create_cell_life}}.
#'
#' @section Error correction:
#' ViSCAR allows users to correct
#' \itemize{
#' \item tracking errors with \code{\link{extract_branch}}, \code{\link{get_cand_mother_cells}} and \code{\link{add_branch}}.
#' \item segmantation errors with \code{\link{split_cell}}, \code{\link{get_cand_merge_cells}} and \code{\link{merge_cells}}.
#' This capability is currently offered to \emph{BaSCA} users \bold{only}.
#' }
#'
#' @section Notes:
#' Some functions have prerequisites in order to be used.
#' See the \emph{Prerequisites} field of each function for more details.
#'
#' @references
#' Balomenos AD, Tsakanikas P, Aspridou Z, Tampakaki A, Koutsoumanis K, Manolakos ES. Image analysis driven single-cell analytics for systems microbiology. BMC Syst Biol. 2017; 11(1).
#' \cr\cr
#' Balomenos AD, Tsakanikas P, Manolakos ES, Tracking single-cells in overcrowded bacterial colonies. 37th Conf Proc IEEE Eng Med Biol Soc. Milan: IEEE; 2015; p. 6473-6476.
#' \cr\cr
#' Balomenos AD, Manolakos ES. Reconstructing the forest of lineage trees of diverse bacterial communities using bio-inspired image analysis. 25th Eur Signal Process Conf, Kos: IEEE; 2017; p. 1887-1891.
#' \cr\cr
#' Balomenos AD, Stefanou V, Manolakos ES. Bacterial Image Analysis and Single-Cell Analytics to Dechipher the Behaviour of Large Microbial Communities. 2018 25th IEEE Int Conf Image Process. IEEE; 2018; p. 2436–2440.
#' \cr\cr
#' \url{http://oufti.org/}
#' \cr\cr
#' \url{http://mtshasta.phys.washington.edu/website/SuperSegger.php}
#'
#' @docType package
#' @name ViSCAR

NULL
