# ViSCAR (Visualization and Single-Cell Analytics using R)

ViSCAR is a set of methods and corresponding functions, to visually explore and correlate single-cell attributes generated from the image processing of complex bacterial single-cell movies. It can be used to model and visualize the spatiotemporal evolution of attributes at different levels of the microbial community organization (i.e., cell population, colony, generation, etc.), to discover possible epigenetic information transfer across cell generations, infer mathematical and statistical models describing various stochastic phenomena (e.g., cell growth, cell division), and even identify and auto-correct errors introduced unavoidably during the bioimage analysis of a dense movie with thousands of overcrowded cells in the microscope's field of view. 


# Software Installation

To install ViSCAR please follow the steps below:

**Step 1**. If R is not installed in your system, please download the latest version from here:

```
https://cran.rstudio.com
```

and install it following the instructions provided.

**Step 2**. Download and install the **R studio** from here:

```
https://rstudio.com/products/rstudio/download/#download
```

following the instructions provided.

**Step 3**. Open the ViSCAR Project with R-Studio by clicking on the file:

```
"ViSCAR.Rproj"
```

located in the current directory.

**Step 4**. To build and install ViSCAR, you have to run the commands below in the R-Studio console (see Figure below).

```
install.packages("devtools")
install.packages("BiocManager")
BiocManager::install("EBImage")
devtools::install()
library('ViSCAR')
```

![](images/Fig1.png)

# ViSCAR Tutorial

Before experimenting with the provided notebooks, you can [download](https://gitlab.com/ManolakosLab/viscar/-/blob/master/images/Tutorial.mp4) and watch the provided tutorial on Case Study I results reproduction.

# Documentation

In the [documentation](https://gitlab.com/ManolakosLab/viscar/-/blob/master/ViSCAR_1.0.0_Documentation.pdf) you can find a description of each ViSCAR's function.

# R notebooks to reproduce case studies

Please install the ViSCAR software following the instructions provided above
before you try to run the R notebooks.

In each directory shown below, we also included for convenience the corresponding notebook
in .html format showing the R function calls and the results it is expected to produce.

- [./Case studies/Case study I](https://gitlab.com/ManolakosLab/viscar/-/tree/master/Case%20studies/Case%20study%20I)
- [./Case studies/Case study II](https://gitlab.com/ManolakosLab/viscar/-/tree/master/Case%20studies/Case%20study%20II)
- [./Case studies/Case study III](https://gitlab.com/ManolakosLab/viscar/-/tree/master/Case%20studies/Case%20study%20III)

Now, to run the R notebooks, please follow the steps below:

**Step 1**. To run Case Study I and produce the results reported in the paper, please open the
notebook file from R-Studio:

```
Case Study I.Rmd
```
This [Rmd file](https://gitlab.com/ManolakosLab/viscar/-/blob/master/Case%20studies/Case%20study%20I/Case%20Study%20I.Rmd) can be found in the directory [./Case studies/Case study I](https://gitlab.com/ManolakosLab/viscar/-/tree/master/Case%20studies/Case%20study%20I).

![](images/Fig2.png)

You can run the whole notebook, as demonstrated in Figure below, to reproduce Case Study
I results. You can also run selectively one or more chunks to reproduce specific results,
but you have to run the first chunk before any other chunk to load the data.

![](images/Fig3.png)

Each chunk's output will be printed on the notebook or saved as a separate figure file
outside R-Studio in the "Case studies" directory; see more details on the notebook's
comments.


**Step 2**. To run Case Study II and produce the results in the paper, please open the notebook file from R-Studio:

```
Case Study II.Rmd
```

This [Rmd file](https://gitlab.com/ManolakosLab/viscar/-/blob/master/Case%20studies/Case%20study%20II/Case%20Study%20II.Rmd) can be found in the directory [./Case studies/Case study II](https://gitlab.com/ManolakosLab/viscar/-/tree/master/Case%20studies/Case%20study%20II).

You can run the whole notebook, as is demonstrated in Figure below, to reproduce Case Study
II results. You can also run selectively one or more chunks to produce specific results, but
you have to run the first chunk to load the data before runnin g any other chunk.

![](images/Fig4.png)

Each chunk's output will be printed on the notebook or saved as a separate figure file
outside R-Studio in the "Case studies" directory; see more details on the notebook's
comments.

**Step 3**. To run Case Study III and produce the results discussed in the paper, please open the notebook file from R-Studio:

```
Case Study III.Rmd
```

This [Rmd file](https://gitlab.com/ManolakosLab/viscar/-/blob/master/Case%20studies/Case%20study%20III/Case%20Study%20III.Rmd) can be found in the directory [./Case studies/Case study III](https://gitlab.com/ManolakosLab/viscar/-/tree/master/Case%20studies/Case%20study%20III).

Before running this case study, please download the dataset (471 MB) from [here](https://drive.google.com/file/d/1YUwgt_HXQvqnIEPT6nkSwYjsGzoYMx9O/view?usp=sharing).


* Unzip the downloaded "Case Study III.zip" file. It contains the input data in Rdata format (i.e., FLT and FDT structure) needed for Case study
  III to be run.
* Transfer the "Case Study III.Rdata" file to the [./Datasets](https://gitlab.com/ManolakosLab/viscar/-/tree/master/Datasets) directory.

You can run the whole notebook, as is demonstrated in Figure below, to reproduce Case Study III results. You can also run selectively one or more chunks to produce specific results, but you have to run the first chunk to load the data before running any other chunk.

![](images/Fig5.png)

Each chunk's output will be printed on the notebook or saved as a separate figure file outside
R-Studio in the "Case studies" directory; see more details on each notebook's comments.

# Contributed Packages

ViSCAR depends on and imports a number of other R-packages:

Depends on 
* R (>= 3.5.1)
* igraph (>= 1.2.1)

Imports 
* data.table (>= 1.11.4)
* ggplot2 (>= 3.0.0),
* graphics 
* grDevices
* stats
* utils
* grid
* EBImage (>= 4.22.0)
* fitdistrplus (>= 1.0-9)
* gplots (>= 3.0.1)
* gridExtra (>= 2.3),
* nlsMicrobio (>= 0.0-1)
* pdist (>= 1.2)
* R.matlab (>= 3.6.1)
* scatterplot3d (>= 0.3-41)
* jsonlite (>= 1.5)

# Contributing

Please read [CONTRIBUTING.md](https://gitlab.com/ManolakosLab/viscar/-/blob/master/CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

# Authors

**Victoria Stefanou** - *Initial work*
**Athanasios D. Balomenos** - *Initial work and Maintainer* - [abalomenos](https://gitlab.com/abalomenos)

# License

This project is licensed under the GPL-2 License - see the [LICENSE.md](https://gitlab.com/ManolakosLab/viscar/-/blob/master/LICENSE.md) file for details

# Publications

* Balomenos AD, Stefanou V, Manolakos ES. Analytics and visualization tools to characterize single-cell stochasticity using 
  bacterial single-cell movie cytometry data. BMC Bioinformatics. 2021 Oct 29;22(1):531. DOI: https://doi.org/10.1186/s12859-021-04409-9 
* Balomenos AD, Stefanou V, Manolakos ES. Bacterial Image Analysis and Single-Cell Analytics to Decipher the Behavior of Large 
  Microbial Communities. In Proceedings of the 25th IEEE International Conference on Image Processing (ICIP); Athens; Oct. 7- 
  10 2018;2436-2440. DOI: https://doi.org/10.1109/ICIP.2018.8451137
* Balomenos AD, Tsakanikas P, Aspridou Z, Tampakaki AP, Koutsoumanis KP, Manolakos ES. Image analysis driven single-cell 
  analytics for systems microbiology. BMC Syst Biol. 2017;11(1):43. DOI: https://doi.org/10.1186/s12918-017-0399-z
  Download BaSCA: https://gitlab.com/ManolakosLab/basca

# Contact

If you have questions / comments on ViSCAR please contact us:

Prof. Elias S. Manolakos

Information Technologies in Medicine and Biology \
Dept. of Informatics and Telecommunications \
University of Athens

eliasm@di.uoa.gr
